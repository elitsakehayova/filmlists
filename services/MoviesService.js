import axios from 'axios';

const apiKey = encodeURIComponent('eeec0fced77a45c82ad18bdd735f25c6');
const baseUrl = 'https://api.themoviedb.org/3';

export const getTopRated = () => axios.get(`${baseUrl}/movie/top_rated?api_key=${apiKey}`).then(res => res.data);

export const getDetailsMovie = movieId => axios.get(`${baseUrl}/movie/${movieId}?api_key=${apiKey}`).then(res => res.data);

export const getAlternativeTitles = () => axios.get(`${baseUrl}/movie/${movie_id}/alternative_titles?api_key=${apiKey}`).then(res => res.data);

export const getCredits = () => axios.get(`${baseUrl}/movie/${movie_id}/credits?api_key=${apiKey}`).then(res => res.data);

export const getImagesMovie = () => axios.get(`${baseUrl}/movie/${movie_id}/images?api_key=${apiKey}`).then(res => res.data);

export const getVideos = () => axios.get(`${baseUrl}/movie/${movie_id}/videos?api_key=${apiKey}`).then(res => res.data);

export const getSimilarMovies = () => axios.get(`${baseUrl}/movie/${movie_id}/similar?api_key=${apiKey}`).then(res => res.data);

export const getReviews = () => axios.get(`${baseUrl}/movie/${movie_id}/reviews?api_key=${apiKey}`).then(res => res.data);

export const getLatestMovie = () => axios.get(`${baseUrl}/movie/latest?api_key=${apiKey}`).then(res => res.data);

export const getNowPlaying = () => axios.get(`${baseUrl}/movie/now_playing?api_key=${apiKey}`).then(res => res.data);

export const getPopularMovie = () => axios.get(`${baseUrl}/movie/popular?api_key=${apiKey}`).then(res => res.data);
