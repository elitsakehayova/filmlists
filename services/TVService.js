import axios from 'axios';

const apiKey = encodeURIComponent('eeec0fced77a45c82ad18bdd735f25c6');
const baseUrl = 'https://api.themoviedb.org/3';

export const getPopularTv = () => axios.get(`${baseUrl}/tv/popular?api_key=${apiKey}`).then(res => res.data);

export const getTopRatedTv = () => axios.get(`${baseUrl}/tv/top_rated?api_key=${apiKey}`).then(res => res.data);

export const getDetailsTv = tvId => axios.get(`${baseUrl}/tv/${tvId}?api_key=${apiKey}`).then(res => res.data);

export const getAccountStates = tv_id => axios.get(`${baseUrl}/tv/${tv_id}/account_states?api_key=${apiKey}`).then(res => res.data);

export const getAlternativeTitlesTv = tv_id => axios.get(`${baseUrl}/tv/${tv_id}/alternative_titles?api_key=${apiKey}`).then(res => res.data);

export const getChanges = tv_id => axios.get(`${baseUrl}/tv/${tv_id}/changes?api_key=${apiKey}`).then(res => res.data);

export const getContentRatings = tv_id => axios.get(`${baseUrl}/tv/${tv_id}/content_ratings?api_key=${apiKey}`).then(res => res.data);

export const getCreditsTv = tv_id => axios.get(`${baseUrl}/tv/${tv_id}/credits?api_key=${apiKey}`).then(res => res.data);

export const getEpisodeGroups = tv_id => axios.get(`${baseUrl}/tv/${tv_id}/episode_groups?api_key=${apiKey}`).then(res => res.data);

export const getExternalIDs = tv_id => axios.get(`${baseUrl}/tv/${tv_id}/external_ids?api_key=${apiKey}`).then(res => res.data);

export const getImagesTv = tv_id => axios.get(`${baseUrl}/tv/${tv_id}/images?api_key=${apiKey}`).then(res => res.data);

export const getKeywords = tv_id => axios.get(`${baseUrl}/tv/${tv_id}/keywords?api_key=${apiKey}`).then(res => res.data);

export const getRecommendations = tv_id => axios.get(`${baseUrl}/tv/${tv_id}/recommendations?api_key=${apiKey}`).then(res => res.data);

export const getReviewsTv = tv_id => axios.get(`${baseUrl}/tv/${tv_id}/reviews?api_key=${apiKey}`).then(res => res.data);

export const getScreenedTheatrically = tv_id => axios.get(`${baseUrl}/tv/${tv_id}/screened_theatrically?api_key=${apiKey}`).then(res => res.data);

export const getSimilarTvShows = tv_id => axios.get(`${baseUrl}/tv/${tv_id}/similar?api_key=${apiKey}`).then(res => res.data);

export const getTranslations = tv_id => axios.get(`${baseUrl}/tv/${tv_id}/translations?api_key=${apiKey}`).then(res => res.data);

export const getVideosTv = tv_id => axios.get(`${baseUrl}/tv/${tv_id}/videos?api_key=${apiKey}`).then(res => res.data);

export const getLatestTv = () => axios.get(`${baseUrl}/tv/latest?api_key=${apiKey}`).then(res => res.data);

export const getTvAiringToday = () => axios.get(`${baseUrl}/tv/airing_today?api_key=${apiKey}`).then(res => res.data);

export const getTvOnTheAir = () => axios.get(`${baseUrl}/tv/on_the_air?api_key=${apiKey}`).then(res => res.data);
