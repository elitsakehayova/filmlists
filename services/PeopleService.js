import axios from 'axios';

const apiKey = encodeURIComponent('eeec0fced77a45c82ad18bdd735f25c6');
const baseUrl = 'https://api.themoviedb.org/3';

export const getDetailsPeople = () => axios.get(`${baseUrl}/person/{person_id}?api_key=${apiKey}`).then(res => res.data);

export const getMovieCredits = () => axios.get(`${baseUrl}/person/{person_id}/movie_credits?api_key=${apiKey}`).then(res => res.data);

export const getTvCredits = () => axios.get(`${baseUrl}/person/{person_id}/tv_credits?api_key=${apiKey}`).then(res => res.data);

export const getCombinedCredits = () => axios.get(`${baseUrl}/person/{person_id}/combined_credits?api_key=${apiKey}`).then(res => res.data);

export const getImagesPeople = () => axios.get(`${baseUrl}/person/{person_id}/images?api_key=${apiKey}`).then(res => res.data);

export const getTaggedImagesPeople = () => axios.get(`${baseUrl}/person/{person_id}/tagged_images?api_key=${apiKey}`).then(res => res.data);

export const getLatestPeople = () => axios.get(`${baseUrl}/person/latest?api_key=${apiKey}`).then(res => res.data);

export const getPopularPeople = () => axios.get(`${baseUrl}/person/popular?api_key=${apiKey}`).then(res => res.data);
