import React from 'react';
import { TouchableOpacity, StyleSheet, ScrollView, Image, View } from 'react-native';
import PropTypes from 'prop-types';
import { getTopRated } from '../services/MoviesService';
import Colors from '../constants/Colors';
import CardDescription from '../components/CardDescription';

export default class LinksScreen extends React.Component {

  static propTypes = {
    navigation: PropTypes.object,
  }

  state = {
    users: []
  }

  componentDidMount = () => {
    getTopRated().then(data => this.setState({ users: data.results }));
  }

  render() {
    return (
      <ScrollView paddingHorizontal={15} style={styles.container}>
        {this.state.users.map(item =>
          <TouchableOpacity key={item.id} onPress={() => this.props.navigation.navigate('DetailPage', { movieId: item.id })}>
            <View flex={1} flexDirection='row' height={280} backgroundColor='#204060' marginBottom={20} borderRadius={40} >
              <View flex={1}>
                <Image source={{ uri: 'http://image.tmdb.org/t/p/w200' + item.poster_path }} style={styles.image} />
              </View>
              <CardDescription movie={item} />
            </View>
          </TouchableOpacity>
        )}
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 30,
    backgroundColor: Colors.backgroundColor
  },
  image: {
    height: 280,
    borderTopLeftRadius: 40,
    borderBottomLeftRadius: 40
  }
});
