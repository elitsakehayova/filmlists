import React from 'react';
import { StyleSheet, ScrollView, TouchableOpacity, Image, View } from 'react-native';
import Colors from '../constants/Colors';
import PropTypes from 'prop-types';
import { getTopRatedTv } from '../services/TVService';
import TvCardDescription from '../components/TvCardDescription';

export default class HomeScreen extends React.Component {

  static propTypes = {
    navigation: PropTypes.object,
  }

  state = {
    tv: []
  }

  componentDidMount = () => {
    getTopRatedTv().then(data => this.setState({ tv: data.results }));
  }

  render() {
    return (
      <ScrollView paddingHorizontal={15} style={styles.container}>
        {this.state.tv.map(item =>
          <TouchableOpacity key={item.id} onPress={() => this.props.navigation.navigate('DetailPageTv', { tvId: item.id })}>
            <View flex={1} flexDirection='row' height={300} backgroundColor='#204060' marginBottom={20} borderRadius={40} >
              <View flex={1}>
                <Image source={{ uri: 'http://image.tmdb.org/t/p/w200' + item.poster_path }} style={styles.image} />
              </View>
              <TvCardDescription tv={item} />
            </View>
          </TouchableOpacity>
        )}
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  image: {
    height: 300,
    borderTopLeftRadius: 40,
    borderBottomLeftRadius: 40
  }
});
