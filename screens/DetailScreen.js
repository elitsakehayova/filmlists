import React from 'react';
import { Text, StyleSheet, View, Image, ScrollView } from 'react-native';
import { Dimensions } from 'react-native';
import PropTypes from 'prop-types';
import { getDetailsMovie } from '../services/MoviesService';
import Colors from '../constants/Colors';
import Raiting from '../components/Raiting';

export default class DetailsMovie extends React.Component {

  static propTypes = {
    navigation: PropTypes.object,
  }

  state = {
    movieDetail: {
      poster_path: '',
      title: '',
      vote_average: '',
      vote_count: '',
      overview: '',
      budget: '',
      release_date: '',
      status: '',
      tagline: '',
      production_companies: [ {
        logo_path: '',
        name: '',
        origin_country: '',
        id: ''
      } ]
    }
  };

  componentDidMount = () => {
    getDetailsMovie(this.props.navigation.getParam('movieId')).then(detail => this.setState({ movieDetail: detail }));
  }

  render() {
    return (
      <ScrollView flex={1} backgroundColor='black' >
        <Image source={{ uri: 'http://image.tmdb.org/t/p/w500' + this.state.movieDetail.poster_path }} style={styles.image} />
        <View flex={1} paddingHorizontal={40}>
          <Text style={styles.title}>{this.state.movieDetail.title}</Text>
          <View flexDirection='row' paddingLeft={60}>
            <Raiting allStars={10} fullStars={Math.round(this.state.movieDetail.vote_average)} />
            <Text style={styles.rates}>{this.state.movieDetail.vote_count + ' Rates'}</Text>
          </View>
          <View>
            <Text style={styles.text}>Storyline</Text>
            <Text style={styles.overview} >{this.state.movieDetail.overview}</Text>
            <Text style={styles.text}>Movie Cast Information</Text>
            <Text style={styles.list}>Budget: {this.state.movieDetail.budget} </Text>
            <Text style={styles.list}>Release Date: {this.state.movieDetail.release_date} </Text>
            <Text style={styles.list}>Status: {this.state.movieDetail.status} </Text>
            <Text style={styles.list}>Tagline: {this.state.movieDetail.tagline} </Text>
          </View>
          <View>
            <Text style={styles.text}>Companies</Text>
            <View flexDirection='row' flexWrap='wrap'>
              {this.state.movieDetail.production_companies.map(company =>
                <View key={company.id} width='50 %' paddingHorizontal={15} >
                  <Image source={{ uri: 'http://image.tmdb.org/t/p/w200' + company.logo_path }} style={styles.logo} />
                  <Text style={styles.companyName}>{company.name}</Text>
                  <Text style={styles.orgCountry}>{company.origin_country}</Text>
                </View>
              )}
            </View>
          </View>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  image: {
    height: Dimensions.get('window').height / 2,
    width: null
  },
  title: {
    textAlign: 'center',
    paddingBottom: 10,
    paddingTop: 10,
    fontSize: 22,
    fontWeight: 'bold',
    color: Colors.white
  },
  rates: {
    fontSize: 16,
    color: Colors.white,
    paddingTop: 10,
    paddingLeft: 10
  },
  text: {
    fontSize: 18,
    color: Colors.white,
    paddingBottom: 10,
    paddingTop: 10
  },
  overview: {
    fontSize: 15,
    color: Colors.white
  },
  list: {
    fontSize: 16,
    color: Colors.white,
  },
  logo: {
    height: 150,
    width: 150
  },
  companyName: {
    fontSize: 16,
    color: Colors.white,
    paddingBottom: 5,
    paddingLeft: 15,
  },
  orgCountry: {
    fontSize: 16,
    color: Colors.white,
    paddingBottom: 5,
    paddingLeft: 20
  }
});
