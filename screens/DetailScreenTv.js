import React from 'react';
import { Text, StyleSheet, View, Image, ScrollView } from 'react-native';
import { Dimensions } from 'react-native';
import PropTypes from 'prop-types';
import { getDetailsTv } from '../services/TVService';
import Colors from '../constants/Colors';
import Raiting from '../components/Raiting';

export default class DetailsTv extends React.Component {

  static propTypes = {
    navigation: PropTypes.object,
  }

  state = {
    tvDetail: {
      poster_path: '',
      backdrop_path: '',
      episode_run_time: '',
      languages: '',
      last_air_date: '',
      last_episode_to_air: [ {
        episode_number: '',
        name: '',
        overview: '',
        season_number: '',
      } ],
      name: '',
      networks: [ {
        name: '',
        id: '',
        logo_path: '',
        origin_country: ''
      } ],
      original_language: '',
      overview: '',
      production_companies: [ {
        logo_path: '',
        name: '',
        origin_country: '',
        id: ''
      } ],
      seasons: [ {
        air_date: '',
        episode_count: '',
        id: '',
        name: '',
        overview: '',
        poster_path: '',
        season_number: ''
      } ],
      vote_average: '',
      vote_count: '',
      status: '',
      type: ''
    }
  };

  componentDidMount = () => {
    getDetailsTv(this.props.navigation.getParam('tvId')).then(detail => this.setState({ tvDetail: detail }));
  }

  render() {
    return (
      <ScrollView flex={1} backgroundColor='black'>
        <Image source={{ uri: 'http://image.tmdb.org/t/p/w500' + this.state.tvDetail.poster_path }} style={styles.image} />
        <View flex={1} paddingHorizontal={40}>
          <Text style={styles.title}>{this.state.tvDetail.name}</Text>
          <View flexDirection='row' paddingLeft={60} paddingBottom={15}>
            <Raiting allStars={10} fullStars={Math.round(this.state.tvDetail.vote_average)} />
            <Text style={styles.rates}>{this.state.tvDetail.vote_count + ' Rates'}</Text>
          </View>
          <View flexDirection='column'>
            <Text style={styles.text}>Duration: {this.state.tvDetail.episode_run_time}</Text>
            <Text style={styles.text}>Language: {this.state.tvDetail.languages}</Text>
            <Text style={styles.text}>Last air date: {this.state.tvDetail.last_air_date.split('-').reverse().join('.')}</Text>
            <Text style={styles.text}>Original language: {this.state.tvDetail.original_language}</Text>
            <Text style={styles.text}>Overview: {this.state.tvDetail.overview}</Text>
          </View>
          <View flexDirection='row' flexWrap='wrap' paddingTop={10} flex={2} paddingBottom={10}>
            <Image source={{ uri: 'http://image.tmdb.org/t/p/w500' + this.state.tvDetail.backdrop_path }} style={styles.ima} />
            <View flexDirection='column' flex={1}>
              <Text style={styles.text}>Season of number: {this.state.tvDetail.last_episode_to_air.season_number}</Text>
              <Text style={styles.text}>Name of last episode: {this.state.tvDetail.last_episode_to_air.name}</Text>
              <Text style={styles.text}>Number of episode: {this.state.tvDetail.last_episode_to_air.episode_number}</Text>
              <Text style={styles.text} numberOfLines={6}>Overview of last episode: {this.state.tvDetail.last_episode_to_air.overview}</Text>
            </View>
          </View>
          <Text style={styles.text}>Seasons:</Text>
          <View flexDirection='row' flexWrap='wrap' paddingTop={10}>
            {this.state.tvDetail.seasons.map(seasons =>
              <View key={seasons.id} width='50 %' paddingHorizontal={15} >
                <Image source={{ uri: 'http://image.tmdb.org/t/p/w200' + seasons.poster_path }} style={styles.logo} />
                <Text style={styles.text}>Name: {seasons.name}</Text>
                <Text style={styles.text}>Season number: {seasons.season_number}</Text>
                <Text style={styles.text}>Air date: {seasons.air_date}</Text>
                <Text style={styles.text}>Episode count: {seasons.episode_count}</Text>
                <Text style={styles.text} numberOfLines={3}>Overview: {seasons.overview}</Text>
              </View>
            )}
          </View>
          <Text style={styles.text}>Networks:</Text>
          <View flexDirection='row' flexWrap='wrap' paddingTop={10}>
            {this.state.tvDetail.networks.map(networks =>
              <View key={networks.id} width='50 %' paddingHorizontal={15} >
                <Image source={{ uri: 'http://image.tmdb.org/t/p/w200' + networks.logo_path }} style={styles.logo} />
                <Text style={styles.companyName}>{networks.name}</Text>
                <Text style={styles.orgCountry}>{networks.origin_country}</Text>
              </View>
            )}
          </View>
          <Text style={styles.text}>Companies:</Text>
          <View flexDirection='row' flexWrap='wrap' paddingTop={10}>
            {this.state.tvDetail.production_companies.map(companies =>
              <View key={companies.id} width='50 %' paddingHorizontal={15}>
                <Image source={{ uri: 'http://image.tmdb.org/t/p/w200' + companies.logo_path }} style={styles.logo} />
                <Text style={styles.companyName}>{companies.name}</Text>
                <Text style={styles.orgCountry}>{companies.origin_country}</Text>
              </View>
            )}
          </View>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  image: {
    height: Dimensions.get('window').height / 2,
    width: null
  },
  title: {
    textAlign: 'center',
    paddingBottom: 10,
    paddingTop: 10,
    fontSize: 22,
    fontWeight: 'bold',
    color: Colors.white
  },
  rates: {
    fontSize: 16,
    color: Colors.white,
    paddingTop: 10,
    paddingLeft: 10
  },
  ima: {
    height: 200,
    width: 180,
  },
  text: {
    fontSize: 18,
    color: Colors.white,
    paddingLeft: 10
  },
  overview: {
    fontSize: 16,
    color: Colors.white
  },
  logo: {
    height: 150,
    width: 150
  },
  companyName: {
    fontSize: 16,
    color: Colors.white,
    paddingBottom: 5,
    paddingLeft: 45,
  },
  orgCountry: {
    fontSize: 16,
    color: Colors.white,
    paddingBottom: 5,
    paddingLeft: 70
  }
});
