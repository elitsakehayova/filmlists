import React from 'react';
import { TouchableOpacity, Text, StyleSheet, Image, View } from 'react-native';
import { genersMovie } from '../constants/GenresMovie';

export default class MovieCard extends React.Component {
  static defaultProps = {
    movies: []
  }

  render() {
    return this.props.movies.map(item =>
      <TouchableOpacity key={item.id} onPress={() => this.props.navigation.navigate('DetailPage', { movieId: item.id })}>
        <View flexDirection='row' height={300} backgroundColor='#204060' marginBottom={50} borderRadius={40} >
          <Image source={{ uri: 'http://image.tmdb.org/t/p/w200' + item.poster_path }} style={styles.image} />
          <View marginLeft={15} width={500}>
            <Text style={styles.title}>{item.title}</Text>
            <View flexDirection='row' >
              <Text style={styles.year}>{item.release_date.split('-')[ 0 ] + ', '}</Text>
              <View flexDirection='row' flexGrow={1} width={0}>
                {item.genre_ids.map(id => <Text key={id} style={styles.geners} >{genersMovie[ id ] + ', '}</Text>)}
              </View>
            </View>
            <Text style={styles.rates}>{item.vote_average}</Text>
            <Text style={styles.overview} >{item.overview}</Text>
          </View>
        </View>
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 30,
    backgroundColor: '#e2e2e2'
  },
  button: {
    borderRadius: 20,
    marginBottom: 15,
    backgroundColor: '#859dc4'
  },
  textButton: {
    textAlign: 'center'
  },
  image: {
    width: 165,
    height: 300,
    borderTopLeftRadius: 40,
    borderBottomLeftRadius: 40
  },
  title: {
    textAlign: 'center',
    paddingRight: 180,
    paddingBottom: 10,
    fontSize: 22,
    fontWeight: 'bold',
    color: 'white'
  },
  year: {
    textAlign: 'left',
    fontSize: 18,
    color: 'white'
  },
  geners: {
    fontSize: 16,
    color: 'white'
  },
  rates: {
    fontSize: 14,
    color: 'white'
  },
  overview: {
    fontSize: 14,
    paddingRight: 172,
    color: 'white'
  }
});
