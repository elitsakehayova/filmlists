import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import PropTypes from 'prop-types';
import Colors from '../constants/Colors';
import { genersMovie } from '../constants/GenresMovie';
import Raiting from './Raiting';

export default class CardDescription extends React.Component {

  static propTypes = {
    movie: PropTypes.object
  };

  render() {
    return (
      <View flex={2} marginLeft={15} >
        <Text style={styles.title}> {this.props.movie.title}</Text>
        <View flexDirection='row'>
          <Text style={styles.year}>{this.props.movie.release_date.split('-')[ 0 ]}</Text>
          <View flexDirection='row' flex={1} flexWrap="wrap">
            {this.props.movie.genre_ids.map((id, index, arr) => <Text key={id} style={styles.geners}>
              {`${genersMovie[ id ]}${arr[ index + 1 ] ? ', ' : ''}`}
            </Text>)}
          </View>
        </View>
        <Raiting allStars={10} fullStars={Math.round(this.props.movie.vote_average)} />
        <Text style={styles.overview}>{this.props.movie.overview}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    textAlign: 'center',
    paddingBottom: 10,
    fontSize: 22,
    fontWeight: 'bold',
    color: Colors.white,
  },
  year: {
    textAlign: 'left',
    fontSize: 18,
    color: Colors.white,
    marginRight: 20
  },
  geners: {
    fontSize: 17,
    color: Colors.white,
  },
  overview: {
    fontSize: 15,
    color: Colors.white,
  }
});

