import React from 'react';
import { StyleSheet, Image, View } from 'react-native';
import PropTypes from 'prop-types';

const fullStar = require('../assets/images/fullStars.png');
const emptyStar = require('../assets/images/emptyStars.png');

export default class Raiting extends React.Component {

  static propTypes = {
    fullStars: PropTypes.number,
    allStars: PropTypes.number,
  };

  render() {
    return (
      <View flexDirection='row' paddingBotton={10}>
        {Array(this.props.allStars).fill().map((stars, index) =>
          <Image source={index < this.props.fullStars ? fullStar : emptyStar} key={index} style={styles.image} />
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  image: {
    height: 30,
    width: 30
  }
});

